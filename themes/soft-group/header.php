<!DOCTYPE>
<html>
  <head>
        <title><?php wp_title(''); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <header id="header">
        <div class="container">
            <div class="col-xs-12">
                <h1><a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h1>
                <p class="description"><?php bloginfo('description'); ?></p>
            </div>
        </div>
    </header>